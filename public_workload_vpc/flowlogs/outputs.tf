output "public_workload_vpc_flow_log_ids" {
  value = [for value in aws_flow_log.workload_vpc_log : value.id]
}