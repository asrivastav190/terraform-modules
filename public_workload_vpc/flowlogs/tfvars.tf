variable "workload_vpc_ids" {
  type = set(string)
}

variable "global" {
  description = "Global map of variables"
  type        = map(string)
  default = {
    "workload_env" = "PublicWorkloadVPC",
    "region"       = "us-east-1"
  }
}

variable "s3_bucket_arn" {}

variable "tags" {
  type = map(string)
}