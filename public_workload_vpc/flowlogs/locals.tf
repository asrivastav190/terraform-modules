locals {
  workload_env = var.global["workload_env"]
  region       = var.global["region"]
  tags         = var.tags
}