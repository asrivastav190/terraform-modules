# ## Workload VPC Flow Log
resource "aws_flow_log" "workload_vpc_log" {
  for_each             = var.workload_vpc_ids
  log_destination      = var.s3_bucket_arn
  log_destination_type = "s3"
  traffic_type         = "ALL"
  vpc_id               = each.value
  tags                 = merge(local.tags, { "Name" = format("%s-VpcFlowLogGroup", local.workload_env) })
}