# AWS Workload VPC
## Domain: Terraform
### Service: VPC
#### Description
●	The Workload VPC consists of four subnets in each AZ:

        > Firewall endpoint
        > AWS Transit Gateway attachment
        > Sharing public subnets 
        > Private subnet to workload account

●	ALB/ELB will be launched in workload account under shared public subnet along with target instance in private shares subnet.
●	This architecture incorporates middlebox routing scenario. For more details , please refer to the below link:
●	A single VPC route table is configured for the return traffic from the firewall endpoint. The route table contains a default route towards the AWS Transit Gateway. 
●	Traffic is returned to the AWS Transit Gateway in the same AZ after it has been inspected by AWS Network Firewall.
#### Terraform Module
##### +---modules
##### |   +---vpc
##### |   >       scripts
##### |   |         +----rt_tag.sh
##### |   |         +----subnet_tag.sh
##### |   |         +----vpc_tag.sh
##### |   |       data.tf
##### |   |       gw.tf
##### |   |       locals.tf
##### |   |       nacl.tf
##### |   |       outputs.tf
##### |   |       providers.tf
##### |   |       route_table.tf
##### |   |       sg.tf
##### |   |       subnets.tf
##### |   |       tfvars.tf
##### |   |       vpc-tgw-attachment.tf
##### |   |       vpc.tf

scripts: This folder contains shell scripts used to copy Name tag to the shared resources like VPC , subnets and route tables. 

gw.tf: This terraform file will create internet gateway which will be attached as route to firewall route table ( as in below screenshot "public_development_vpc-NFW-RTPublic" ) and attached to edge association of middlebox routing.
![Image](images/vpc/pub_vpce_edge.PNG)
  
locals.tf: Locals are named values that you can refer to in your configuration. In this case we are passing environment name, region, tag values, subnet name and availability zones.

nacl.tf: Used to create a private NACL for the workload private share subnet where backend server will be launched.

outputs.tf: This is responsible for logging the output of the workload VPC ID, it’s subnets, VPC flow log ID and attachment ID of VPC and Transit Gateway.

route_table.tf: This Terraform file is responsible for creating firewall , private share , private transit for all AZs. Public share route is created for each AZ associating firewall endpoint as route and each AZ subnt to it. Below are the reference screenshots:

 ![Image](images/vpc/rt_1.png)
 ![Image](images/vpc/rt_2.png)
 ![Image](images/vpc/rt_3.png)
 ![Image](images/vpc/rt_4.png)
 ![Image](images/vpc/rt_5.png)
 ![Image](images/vpc/rt_6.png)

sg.tf: Defines the allowed security groups required for the Inspection VPC.

subnets.tf: This Terraform file creates firewall subnet,public shared subnet , private transit subnet and private share subnet for the workload VPC according to its defined AZs. Below is a reference screenshot:
 
![Image](images/vpc/subnets.png)

tfvars.tf: This Terraform file is used to pass input variables like the workload VPC CIDR range, tags, environment, availability zone, Transit Gateway ID, its route table and NACL rules. All these parameters are passed by the root folder variables.tf file.

vpc-tgw-attachement.tf: This file is responsible for creating the attachment between the workload VPC and Transit Gateway, it then associates the attachment to the firewall route table of the Transit Gateway.

vpc.tf: Terraform file used to create the public/private workload VPC and it's flow log.
####  Root Structure
##### | |   main.tf
##### | |   variables.tf
##### | |   outputs.tf

main.tf: This file is responsible for passing variables required to call the public/private workload vpc module and create its resources as defined above in a specified AWS account.

variables.tf: Variables for public and private NACL rules passed specifically for the workload vpc module. Please note all of these variables can be customized, see screenshot below.

 ![Image](images/vpc/var.png)

#### Reference Links

Middlebox Routing: https://docs.aws.amazon.com/vpc/latest/userguide/gwlb-route.html
AWS CLI commands : https://docs.aws.amazon.com/cli/latest/userguide/cli-usage-filter.html


#### Diagram
 ![Image](images/vpc/arch.png)

#### Limitations
N/A

---
___
