# data "aws_region" "current" {}
# data "aws_vpc" "default" {
#   id    = var.vpc_id
# }
data "aws_subnets" "ids" {
  filter {
    name   = "tag:Name"
    values = ["*-share-*"] # insert values here
  }
}

data "aws_subnet" "share_subnet_arn" {
  #count = length(data.aws_subnets_ids.share_subnets.ids)
  for_each = toset(data.aws_subnets.ids.ids)
  #id = data.aws_subnets_ids.share_subnets[count.index].ids
  id = each.value
#   filter {
#     name   = "tag:Name"
#     values = ["*-share-*"]
#   }
}

output "arn" {
  value = [for v in data.aws_subnet.share_subnet_arn : v.arn]
}

# output "vpc_cidr_out" {
#     value = data.aws_vpc.default.cidr_block
# }