resource "aws_ram_resource_share" "main" {
  name                      = var.ram_name
  allow_external_principals = true

  tags                 = var.tags
}

resource "aws_ram_principal_association" "main" {
  principal          = var.workload_account_id
  resource_share_arn = aws_ram_resource_share.main.arn
}

resource "aws_ram_resource_association" "subnet_share" {
  for_each              = toset([for v in data.aws_subnet.share_subnet_arn : v.arn])
  resource_arn       = each.value
  #resource_arn       = data.aws_subnet.share_subnet_arn[count.index].arn
  resource_share_arn = aws_ram_resource_share.main.arn
}

output "aws_ram_resource_id" {
  value = [for value in aws_ram_resource_association.subnet_share : value.id]
}

output "ram_id" {
  value = aws_ram_resource_share.main.id
}