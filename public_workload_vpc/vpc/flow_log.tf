
# ## Workload VPC Flow Log
resource "aws_flow_log" "workload_vpc_log" {
  count = var.public_flow_log_enable ? 1 : 0
  log_destination      = var.s3_bucket_arn
  log_destination_type = "s3"
  traffic_type         = "ALL"
  vpc_id               = aws_vpc.iso_ne_workload_vpc.id
  tags                 = merge(var.tags, { "Name" = format("%s-FlowLogGroup", var.vpc_name) })
}