variable "subnet_name" {}
variable "subnets" {}
variable "vpc_id" {}
variable "vpc_name" {
  type        = string
  description = "Name for the requested VPC"
}

variable "map_public_ip_on_launch" {}

variable "tags" {
  type = map(string)
}
