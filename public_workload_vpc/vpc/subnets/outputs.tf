output "subnet_ids" {
    value = [ for subnet in aws_subnet.default : subnet.id]
}

output "subnet_arn" {
    value = [ for subnet in aws_subnet.default : subnet.arn]
}