output "vpc_id" {
  value       = aws_vpc.iso_ne_workload_vpc.id
  description = "The ID of the VPC"
}
output "prv_tgw_subnet_ids" {
  value = [for subnet in aws_subnet.prv_tgw : subnet.id]
  description = "Subnet IDs for the /28 subnet for Transit Gateway attachment"
}

output "igw_id" {
  value = aws_internet_gateway.igw.id
}

output "igw_arn" {
  value = aws_internet_gateway.igw.arn
}