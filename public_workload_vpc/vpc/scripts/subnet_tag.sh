#!/bin/bash


echo "Running subnet script....."
aws ec2 describe-subnets --filter "Name=vpc-id,Values=$1" --query 'Subnets[*].[SubnetId]' --region us-east-1 --profile pwc_explore --output text | while read line;do
  # reading each line
  sub_id=`echo $line | sed 's/ //g'`
  echo "Subnet ID: $sub_id"
  echo "1.) Checking tag:Name."

  tag_name_val=`aws ec2 describe-subnets --filter "Name=vpc-id,Values=$1" --region us-east-1 --query "Subnets[?SubnetId=='$sub_id'].[Tags[?Key=='Name'].[Value]]" --output text --profile pwc_explore`
  tag_name=`aws ec2 describe-subnets --filter "Name=vpc-id,Values=$1" --region us-east-1 --query "Subnets[?SubnetId=='$sub_id'].[Tags[?Key=='Name'].[Value]]" --output text --profile pwc_explore`
  echo $tag_name_val
  
  #check name tag.
  if [ -z $tag_name_val ]|| [ $tag_name_val == "None" ] ; then
    echo "No name tag. Adding ......"
	## store tag name value for subnets.
    name_tag=`aws ec2 describe-tags --filters "Name=resource-type,Values=subnet" "Name=resource-id,Values=$sub_id"  "Name=key,Values=Name" --region us-east-1 --query 'Tags[*].Value' --output text --profile default`
    aws ec2 create-tags --resources $sub_id --tags Key=Name,Value=$name_tag --region us-east-1 --profile pwc_explore
    echo "Finished adding name tag."
  else
    echo "Name tag exists."
  fi
  
  echo "2.) Checking tag:iso-environment."
  tag_env_val=`aws ec2 describe-subnets --filter Name=vpc-id,Values=$1 --region us-east-1 --query "Subnets[?SubnetId=='$sub_id'].[Tags[?Key=='iso-environment'].[Value]]" --output text --profile pwc_explore`
  #check env tag.
  if [ -z $tag_env_val ]|| [ $tag_env_val == "None" ] ; then
    echo "No env tags. Adding ..."
	## store tag name value for subnets.
    env_tag=`aws ec2 describe-tags --filters "Name=resource-type,Values=subnet" "Name=resource-id,Values=$sub_id"  "Name=key,Values=iso-environment" --region us-east-1 --query 'Tags[*].Value' --output text --profile default`
    aws ec2 create-tags --resources $sub_id --tags Key=iso-environment,Value=$env_tag --region us-east-1 --profile pwc_explore
    echo "Finished adding env tag."
  else
    echo "Env tag exists."
  fi
done