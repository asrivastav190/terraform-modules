#!/bin/bash

echo "Running  vpc script....."
echo $1

printenv | grep "AWS_ACCESS_KEY_ID"

management() {
  echo "Inside management"
  echo $AWS_ACCESS_KEY_ID
  unset AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY
  export "AWS_ACCESS_KEY_ID=$2"
  export "AWS_SECRET_ACCESS_KEY=$3"
  printenv | grep "AWS_ACCESS_KEY_ID"
}

sandbox(){
  echo "Inside sanbox"
  echo $AWS_ACCESS_KEY_ID
  unset AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY
  export "AWS_ACCESS_KEY_ID=$4"
  export "AWS_SECRET_ACCESS_KEY=$5"
  printenv | grep "AWS_ACCESS_KEY_ID"
}

management()
echo "check if management access key"
printenv | grep "AWS_ACCESS_KEY_ID"
tag_name_val=`aws ec2 describe-tags --filters "Name=resource-type,Values=vpc" "Name=resource-id,Values=$1"  "Name=key,Values=Name" --region us-east-1 --query "Tags[?Key=='Name'].Value" --output text --profile pwc_explore`

if [ -z $tag_name_val ]; then
   echo "No name tag. Adding ......"
   sandbox()
  #  unset AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY
  #  export "AWS_ACCESS_KEY_ID=$4"
  #  export "AWS_SECRET_ACCESS_KEY=$5"
   printenv | grep "AWS_ACCESS_KEY_ID"
   name_tag=`aws ec2 describe-tags --filters "Name=resource-type,Values=vpc" "Name=resource-id,Values=$1"  "Name=key,Values=Name" --region us-east-1 --query 'Tags[*].Value' --output text --profile default`
  #  unset AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY
  #  export "AWS_ACCESS_KEY_ID=$2"
  #  export "AWS_SECRET_ACCESS_KEY=$3"
  management()
   aws ec2 create-tags --resources $1 --tags Key=Name,Value=$name_tag --region us-east-1 --profile pwc_explore
   echo "Finished adding name tag."
 else
   echo "Name tag exists."
fi


tag_env_val=`aws ec2 describe-tags --filters "Name=resource-type,Values=vpc" "Name=resource-id,Values=$1"  "Name=key,Values=iso-environment" --region us-east-1 --query "Tags[?Key=='iso-environment'].Value" --output text --profile pwc_explore`

if [ -z $tag_env_val ]; then
   echo "No env tag. Adding ......"
   sandbox()
  # unset AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY
  #  export "AWS_ACCESS_KEY_ID=$4"
  #  export "AWS_SECRET_ACCESS_KEY=$5"
   env_tag=`aws ec2 describe-tags --filters "Name=resource-type,Values=vpc" "Name=resource-id,Values=$1"  "Name=key,Values=iso-environment" --region us-east-1 --query 'Tags[*].Value' --output text --profile default`
  #  unset AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY
  #  export "AWS_ACCESS_KEY_ID=$2"
  #  export "AWS_SECRET_ACCESS_KEY=$3"
   management()
   aws ec2 create-tags --resources $1 --tags Key=iso-environment,Value=$env_tag --region us-east-1 --profile pwc_explore
   echo "Finished adding env tag."
 else
   echo "Env tag exists."
fi