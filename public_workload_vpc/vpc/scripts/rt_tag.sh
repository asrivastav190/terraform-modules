#!/bin/bash

echo "Running route table script....."

aws ec2 describe-route-tables --filter "Name=vpc-id,Values=$1" --region us-east-1 --query 'RouteTables[*].Associations[].[RouteTableId]' --output text --profile pwc_explore | while read line;do
 # reading each line
 rt_id=`echo $line | sed 's/ //g'`
 echo "RT ID: $rt_id"
 echo "1.) Checking tag:Name."
 tag_name_val=`aws ec2 describe-route-tables --filter Name=vpc-id,Values=$1 --region us-east-1 --query "RouteTables[*].[Associations[?RouteTableId=='$rt_id'] && Tags[?Key=='Name'].Value]" --profile pwc_explore --output text`
 echo $tag_name_val
 #check name tag.
 if [[ -z $tag_name_val ]]; then
   echo "No name tag. Adding ......"
   ## store tag name value for subnets.
   name_tag=`aws ec2 describe-tags --filters "Name=resource-type,Values=route-table" "Name=resource-id,Values=$line"  "Name=key,Values=Name" --region us-east-1 --query 'Tags[*].Value' --output text --profile default`
   aws ec2 create-tags --resources $rt_id --tags Key=Name,Value=$name_tag --region us-east-1 --profile pwc_explore
   echo "Finished adding name tag."
 else
   echo "Name tag exists."
 fi
 
 echo "2.) Checking tag:iso-environment."
   
 tag_env_val=`aws ec2 describe-tags --filters "Name=resource-type,Values=route-table" "Name=resource-id,Values=$line"  "Name=key,Values=iso-environment" --region us-east-1 --query 'Tags[*].Value' --output text --profile pwc_explore`
  #check env tag.
 if [[ -z $tag_env_val ]]; then
   echo "No env tags. Adding ..."
   env_tag=`aws ec2 describe-tags --filters "Name=resource-type,Values=route-table" "Name=resource-id,Values=$line"  "Name=key,Values=iso-environment" --region us-east-1 --query 'Tags[*].Value' --output text --profile default`
   aws ec2 create-tags --resources $rt_id --tags Key=iso-environment,Value=$env_tag --region us-east-1 --profile pwc_explore
   echo "Finished adding env tag."
  else
    echo "Env tag exists."
  fi
  sleep 5
done