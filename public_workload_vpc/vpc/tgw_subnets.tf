resource "aws_subnet" "prv_tgw" {
  count                   = "${length(var.tgw_subnets)}"  
  vpc_id                  = aws_vpc.iso_ne_workload_vpc.id
  cidr_block              = "${var.cidr_ab}.${element(values(var.tgw_subnets), count.index)}"
  map_public_ip_on_launch = var.map_public_ip_on_launch
  availability_zone_id    = "${element(keys(var.tgw_subnets), count.index)}"
  tags = merge(
    var.tags,
    {
      "Name" = format("%s-%s",local.prv_tgw_subnet , count.index+1)
    }
  )
}
