locals {
  vpc_cidr       = "${var.cidr_ab}.0.0/16"
  region         = "us-east-1"
  prv_tgw_subnet = format("%s-SN-Prv-TGW", var.vpc_name)
}
