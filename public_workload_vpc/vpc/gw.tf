# /*
# This terraform template will create the following resources,
# 1. Internet gateway
# */

# # create workload vpc Internet gateway 
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.iso_ne_workload_vpc.id
  tags   = merge(var.tags, { "Name" = format("%s-IGW", var.vpc_name) })
}