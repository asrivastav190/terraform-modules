# Create Workload VPC
resource "aws_vpc" "iso_ne_workload_vpc" {
  cidr_block           = local.vpc_cidr
  enable_dns_support   = var.enable_dns_support
  enable_dns_hostnames = var.enable_dns_hostnames
  tags                 = merge(var.tags, { "Name" = var.vpc_name})
}