resource "aws_route_table" "main" {
  vpc_id = var.vpc_id
  tags = merge(var.tags, { "Name" = "${var.vpc_name}-RT-${var.rt_suffix}" })
}

resource "aws_route_table_association" "main" {
  count          = var.rt_suffix != null ? length(var.subnet_count) : 0
  #count          = length(var.subnet_count)
  subnet_id      = element(tolist(data.aws_subnet_ids.main.ids),count.index)
  route_table_id = aws_route_table.main.id
}

resource "aws_route_table_association" "pub_vpce_share" {
 count          = var.rt_suffix == "pub" ? 1 : 0
 
  gateway_id     = join(",",matchkeys(values(var.gateway_id),keys(var.gateway_id),[var.vpc_name]))
  route_table_id = aws_route_table.main.id
}