output "rt_id" {
  value       = aws_route_table.main.id
  description = "Workload VPC route table identifier"
}