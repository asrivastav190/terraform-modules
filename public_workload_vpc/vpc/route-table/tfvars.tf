variable "tags" {
  type = map(string)
}

variable "vpc_id" {}
variable "vpc_name" {
  type        = string
  description = "Name for the requested VPC"
}

variable "rt_suffix" {}
variable "sub_suffix" {}
variable "gateway_id" {}
variable "subnet_count" {}