data "aws_region" "current" {}

data "aws_subnet_ids" "main" {
  vpc_id = var.vpc_id
  tags = {
    Name = "*-${var.sub_suffix}-*"
  }
}

data "aws_internet_gateway" "main" {
  filter {
    name   = "attachment.vpc-id"
    values = [var.vpc_id]
  }
}