resource "aws_route" "tgw" {
 count                     = var.rt_suffix == "TGW" ? 1 : 0
  route_table_id            = aws_route_table.main.id
  destination_cidr_block    = "0.0.0.0/0"
  transit_gateway_id        = var.gateway_id
}

resource "aws_route" "prv_share" {
 count                     = var.rt_suffix == "Share-private" ? 1 : 0
  route_table_id            = aws_route_table.main.id
  destination_cidr_block    = "0.0.0.0/0"
  transit_gateway_id        = var.gateway_id
}


resource "aws_route" "pub_share" {
  count                     = var.rt_suffix == "Share-public" ? 1 : 0
  route_table_id            = aws_route_table.main.id
  destination_cidr_block    = "0.0.0.0/0"
  gateway_id                = join(",",matchkeys(values(var.gateway_id),keys(var.gateway_id),[var.vpc_name]))
}