variable "vpc_id" {}
variable "vpc_name" {}
variable "transit_gateway_id" {
  type        = string
  description = "Identifier for the Transit Gateway that will be associated with this VPC"
}

variable "spoke_vpc_inspection_tgw_route_table_id" {
  type        = string
  description = "Transit Gateway Route Table Identifier for Spoke VPC Inpection TGW Route Table"
}

variable "firewall_subnet_tgw_route_table_id" {
  type        = string
  description = "Transit Gateway Route Table Identifier for Firewall Sunbnet TGW Route Table"
}
variable "tags" {
  type    = map(string)
}