resource "aws_ec2_transit_gateway_vpc_attachment" "public_spoke_vpc_attachment" {
  subnet_ids         = data.aws_subnet_ids.tgw.ids
  transit_gateway_id = var.transit_gateway_id
  vpc_id             = var.vpc_id
  transit_gateway_default_route_table_association = false
  transit_gateway_default_route_table_propagation = false
  tags                 = merge(var.tags, { "Name" = var.vpc_name})
}

# # # Associate the above VPC attachment with the Route Table of Transit Gateway

resource "aws_ec2_transit_gateway_route_table_association" "tgw_rt_association" {
  transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.public_spoke_vpc_attachment.id
  transit_gateway_route_table_id = var.spoke_vpc_inspection_tgw_route_table_id
}

# # # Propagate the Route for the destination VPC CIDR in the Route Table

resource "aws_ec2_transit_gateway_route_table_propagation" "tgw_rt_propagation" {
  transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.public_spoke_vpc_attachment.id
  transit_gateway_route_table_id = var.firewall_subnet_tgw_route_table_id
}
