# These Input variables are expected to be passed from Calling Module
variable "enable_dns_support" {
  type        = bool
  description = "A boolean flag to enable/disable DNS support in the VPC"
  default     = true
}
variable "enable_dns_hostnames" {
  type        = bool
  description = "A boolean flag to enable/disable DNS hostnames in the VPC"
  default     = true
}
variable "vpc_name" {
  type        = string
  description = "Name for the requested VPC"
}

variable "cidr_ab" {
  type = string
  description = "CIDR block for requested VPC"
}

variable "tgw_subnets" {}

variable "tags" {
  type    = map(string)
}

variable "map_public_ip_on_launch" {}
variable "public_flow_log_enable" {}
variable "s3_bucket_arn" {}