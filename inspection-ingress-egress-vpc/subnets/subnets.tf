resource "aws_subnet" "default" {
  count                   = "${length(var.subnets)}"  
  vpc_id                  = var.vpc_id
  cidr_block              = "${substr(data.aws_vpc.default.cidr_block,0,6)}.${element(values(var.subnets), count.index)}"
  map_public_ip_on_launch = var.map_public_ip_on_launch
  availability_zone_id    = "${element(keys(var.subnets), count.index)}"
  tags = merge(
    var.tags,
    {
      "Name" = format("%s-%s",var.subnet_name, count.index+1)
    }
  )
}
