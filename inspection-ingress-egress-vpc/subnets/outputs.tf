output "subnet_ids" {
  value = [for value in aws_subnet.default : value.id]
}

# output "inspection_nfw_subnet_ids" {
#   value = [for value in aws_subnet.inspection_prv_nfw : value.id]
# }