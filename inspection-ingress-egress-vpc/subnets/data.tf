data "aws_region" "current" {}
data "aws_vpc" "default" {
  id    = var.vpc_id
}

output "cidr_ab" {
  value = substr(data.aws_vpc.default.cidr_block,0,6)
}

output "vpc_cidr_out" {
    value = data.aws_vpc.default.cidr_block
}