variable "subnet_name" {}
variable "subnets" {}
variable "vpc_id" {}
variable "map_public_ip_on_launch" {}

variable "tags" {
  type = map(string)
}

# variable "inspection_nfw_subnets" {}
# variable "inspection_vpc_id" {}