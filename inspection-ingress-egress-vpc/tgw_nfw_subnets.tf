resource "aws_subnet" "prv_tgw" {
  count                   = "${length(var.inspection_ingress_egress_tgw_subnets)}"  
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "${var.cidr_ab}.${element(values(var.inspection_ingress_egress_tgw_subnets), count.index)}"
  map_public_ip_on_launch = var.map_public_ip_on_launch
  availability_zone_id    = "${element(keys(var.inspection_ingress_egress_tgw_subnets), count.index)}"
  tags = merge(
    var.tags,
    {
      "Name" = format("%s-%s",local.prv_tgw_subnet , count.index+1)
    }
  )
}

# data "aws_vpc" "inspection" {
#   id    = var.inspection_vpc_id
# }

resource "aws_subnet" "inspection_prv_nfw" {
  count                   = var.vpc_name == "inspection_vpc" ? length(var.inspection_nfw_subnets) : 0
  vpc_id                  = aws_vpc.main.id
  cidr_block              = "${var.cidr_ab}.${element(values(var.inspection_nfw_subnets), count.index)}"
  map_public_ip_on_launch = var.map_public_ip_on_launch
  availability_zone_id    = "${element(keys(var.inspection_nfw_subnets), count.index)}"
  tags = merge(
    var.tags,
    {
      "Name" = format("%s-%s",local.prv_nfw_inspection , count.index+1)
    }
  )
}