variable "inspection_ingress_egress_tgw_subnets" {}

variable "global" {
  description = "Global map of variables"
  type        = map(string)
  default = {
    "inspection_env" = "InspectionVPC",
    "ingress_env"    = "IngressVPC",
    "egress_env"     = "EgressVPC"
    "region"         = "us-east-1"
  }
}

variable "vpc_name" {
  type        = string
  description = "Name for the requested VPC"
}


variable "tags" {
  type = map(string)
}

variable "cidr_ab" {
  type        = string
  description = "CIDR block for requested VPC"
}

variable "flow_log_enable" {}

variable "enable_dns_support" {
  type        = bool
  description = "A boolean flag to enable/disable DNS support in the VPC"
  default     = true
}
variable "enable_dns_hostnames" {
  type        = bool
  description = "A boolean flag to enable/disable DNS hostnames in the VPC"
  default     = true
}

variable "s3_bucket_arn" {}

variable "map_public_ip_on_launch" {}


variable "inspection_nfw_subnets" {}