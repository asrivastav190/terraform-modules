output "vpc_id" {
  value = aws_vpc.main.id
}

output "prv_tgw_subnet_ids" {
  value = [for value in aws_subnet.prv_tgw : value.id]
}

output "inspection_nfw_subnet_ids" {
  value = [for value in aws_subnet.inspection_prv_nfw : value.id]
}