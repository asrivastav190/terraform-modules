# ### This default route in Spoke Inspection TGW Route Table will route traffic generated from spoke VPC to Inspection VPC
resource "aws_ec2_transit_gateway_route" "default_inspection_vpc_route" {
  destination_cidr_block         = "0.0.0.0/0"
  transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.inspection_vpc_tgw_attachment.id
  transit_gateway_route_table_id =  var.spoke_vpc_inspection_tgw_route_table_id
}

# ###This default route in Firewall TGW Route Table will route traffic to central egress VPC from firewall VPC
resource "aws_ec2_transit_gateway_route" "default_egress_vpc_route" {
  destination_cidr_block         = "0.0.0.0/0"
  transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.egress_vpc_tgw_attachment.id
  transit_gateway_route_table_id = var.firewall_subnet_tgw_route_table_id
}