# ### Inspection VPC
resource "aws_ec2_transit_gateway_vpc_attachment" "inspection_vpc_tgw_attachment" {
  subnet_ids                                      = tolist(data.aws_subnet_ids.inspection_tgw.ids)
  transit_gateway_id                              = var.transit_gateway_id
  vpc_id                                          = var.inspection_vpc_id
  transit_gateway_default_route_table_association = false
  transit_gateway_default_route_table_propagation = false
}

# # Associate the above VPC attachment with the Route Table of Transit Gateway

resource "aws_ec2_transit_gateway_route_table_association" "inspection_vpc_tgw" {
  transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.inspection_vpc_tgw_attachment.id
  transit_gateway_route_table_id = var.firewall_subnet_tgw_route_table_id
}

# # Propagate the Route for the destination VPC CIDR in the Route Table.
# # Inspection VPC CIDR need not to be propagated in Transit Gateway Route Table
# /*
# resource "aws_ec2_transit_gateway_route_table_propagation" "inspection_vpc_tgw" {
#   transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.inspection_vpc_tgw_attachment.id
#   transit_gateway_route_table_id = var.transit_gateway_route_table_id
# }*/

# ### Ingress VPC
resource "aws_ec2_transit_gateway_vpc_attachment" "ingress_vpc_tgw_attachment" {
  subnet_ids                                      = tolist(data.aws_subnet_ids.ingress_tgw.ids)
  transit_gateway_id                              = var.transit_gateway_id
  vpc_id                                          = var.ingress_vpc_id
  transit_gateway_default_route_table_association = false
  transit_gateway_default_route_table_propagation = false
#   depends_on                                      = [aws_subnet.prv_tgw_ingress]
}

# # Ingress VPC attachment will be associated with Spoke VPC Inspection TGW route table

resource "aws_ec2_transit_gateway_route_table_association" "ingress_vpc_tgw" {
  transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.ingress_vpc_tgw_attachment.id
  transit_gateway_route_table_id = var.spoke_vpc_inspection_tgw_route_table_id
}

# Ingress VPC CIDR route will be propgated in Firewall Subnet TGW Route Table

resource "aws_ec2_transit_gateway_route_table_propagation" "ingress_vpc_tgw" {
  transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.ingress_vpc_tgw_attachment.id
  transit_gateway_route_table_id = var.firewall_subnet_tgw_route_table_id
}

# ### Egress VPC
resource "aws_ec2_transit_gateway_vpc_attachment" "egress_vpc_tgw_attachment" {
  subnet_ids                                      = tolist(data.aws_subnet_ids.egress_tgw.ids)
  transit_gateway_id                              = var.transit_gateway_id
  vpc_id                                          = var.egress_vpc_id
  transit_gateway_default_route_table_association = false
  transit_gateway_default_route_table_propagation = false
#   depends_on                                      = [aws_subnet.prv_tgw_egress]
}

# # Egress VPC attachement will be associated with Spoke VPC Inspection TGW route table

resource "aws_ec2_transit_gateway_route_table_association" "egress_vpc_tgw" {
  transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.egress_vpc_tgw_attachment.id
  transit_gateway_route_table_id = var.spoke_vpc_inspection_tgw_route_table_id
}

# # Egress VPC CIDR route need not be propagted.
# /*
# resource "aws_ec2_transit_gateway_route_table_propagation" "egress_vpc_tgw" {
#   transit_gateway_attachment_id  = aws_ec2_transit_gateway_vpc_attachment.egress_vpc_tgw_attachment.id
#   transit_gateway_route_table_id = var.transit_gateway_route_table_id
# }*/