variable "transit_gateway_id" {}
variable "spoke_vpc_inspection_tgw_route_table_id" {
  type        = string
  description = "Transit Gateway Route Table Identifier for Spoke VPC Inpection TGW Route Table"
}

variable "firewall_subnet_tgw_route_table_id" {
  type        = string
  description = "Transit Gateway Route Table Identifier for Firewall Sunbnet TGW Route Table"
}

variable "inspection_vpc_id" {}
variable "ingress_vpc_id" {}
variable "egress_vpc_id" {}
