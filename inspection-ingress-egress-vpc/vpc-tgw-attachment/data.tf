data "aws_region" "current" {}

data "aws_subnet_ids" "inspection_tgw" {
  vpc_id = var.inspection_vpc_id
  tags = {
    Name = "*-TGW-*"
  }
}

data "aws_subnet_ids" "ingress_tgw" {
  vpc_id = var.ingress_vpc_id
  tags = {
    Name = "*-TGW-*"
  }
}

data "aws_subnet_ids" "egress_tgw" {
  vpc_id = var.egress_vpc_id
  tags = {
    Name = "*-TGW-*"
  }
}