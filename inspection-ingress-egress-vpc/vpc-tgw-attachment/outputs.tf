output "inspection_vpc_tgw_attachment_id" {
  value       = aws_ec2_transit_gateway_vpc_attachment.inspection_vpc_tgw_attachment.id
  description = "Transit gateway VPC identifier for Inspection VPC attachment"
}

output "ingress_vpc_tgw_attachment_id" {
  value       = aws_ec2_transit_gateway_vpc_attachment.ingress_vpc_tgw_attachment.id
  description = "Transit gateway VPC identifier for Ingress VPC attachment"
}

output "central_egress_vpc_tgw_attachment_id" {
  value       = aws_ec2_transit_gateway_vpc_attachment.egress_vpc_tgw_attachment.id
  description = "Transit gateway VPC identifier for Egress VPC attachment"
}