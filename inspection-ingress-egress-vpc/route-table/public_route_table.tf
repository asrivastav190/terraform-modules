resource "aws_route_table" "public" {
  count                   = var.env == "egress_vpc" || var.env == "ingress_vpc"  ? 1 : 0
  vpc_id                  = var.vpc_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw[0].id
  }
  route {
    cidr_block         = "10.0.0.0/8"
    transit_gateway_id = var.transit_gateway_id
  }
  tags   = merge(var.tags, { "Name" = "${var.env}-RT-Public" })
}

resource "aws_route_table_association" "public" {
  count          = var.env == "egress_vpc" || var.env == "ingress_vpc"  ? length(var.pub_count) : 0
  subnet_id      = element(tolist(data.aws_subnet_ids.pub[0].ids),count.index)
  route_table_id = aws_route_table.public[0].id
}