resource "aws_route_table" "inspection_prv_nfw" {
  count                   = var.env == "inspection_vpc"  ? 1 : 0
  vpc_id                  = var.vpc_id
  route {
    cidr_block         = "0.0.0.0/0"
    transit_gateway_id = var.transit_gateway_id
  }
  tags   = merge(var.tags, { "Name" = "${var.env}-RT-PrivateNFW" })
}

resource "aws_route_table_association" "inspection_prv_nfw" {
  count          = var.env == "inspection_vpc" ? length(var.nfw_count) : 0
  subnet_id      = element(tolist(data.aws_subnet_ids.nfw[0].ids),count.index)
  route_table_id = aws_route_table.inspection_prv_nfw[0].id
}
