data "aws_region" "current" {}
data "aws_subnet_ids" "tgw" {
  vpc_id = var.vpc_id
  tags = {
    Name = "*-TGW-*"
  }
}

data "aws_subnet_ids" "nfw" {
  count = var.env == "inspection_vpc"  ? 1: 0
  vpc_id =  var.vpc_id
  tags = {
    Name = "*-NFW-*"
  }
}

data "aws_subnet_ids" "pub" {
  count = var.env != "inspection_vpc" ? 1 : 0
  vpc_id = var.vpc_id
  tags = {
    Name = "*-pub-*"
  }
}

data "aws_subnet_ids" "egress_pub" {
  #vpc_id = data.aws_vpc.egress.id
  count = var.env == "egress_vpc" ? 1 : 0 
  vpc_id = var.vpc_id
  tags = {
    Name = "*-pub-*"
  }
}