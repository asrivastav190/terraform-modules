# # # ## Private TGW RT

resource "aws_route_table" "prv_tgw" {
  vpc_id = var.vpc_id
  tags   = merge(var.tags, { "Name" = "${var.env}-RT-PrivateTGW" })
}

resource "aws_route_table_association" "prv_tgw" {
  count          = length(var.tgw_count)
  subnet_id      = element(tolist(data.aws_subnet_ids.tgw.ids),count.index)
  route_table_id = aws_route_table.prv_tgw.id
}

##uncomment after vpc-tgw attachment.
resource "aws_route" "ingress_egress_tgw" {
  count                     = var.env == "egress_vpc" || var.env == "ingress_vpc"  ? 1 : 0
  route_table_id            = aws_route_table.prv_tgw.id
  destination_cidr_block    = "10.0.0.0/8"
  transit_gateway_id        = var.transit_gateway_id
}

resource "aws_route" "egress_tgw" {
  count                     = var.env == "egress_vpc" ? 1 : 0
  route_table_id            = aws_route_table.prv_tgw.id
  destination_cidr_block    = "0.0.0.0/0"
  nat_gateway_id            = element([for value in aws_nat_gateway.nat : value.id], 0)
}