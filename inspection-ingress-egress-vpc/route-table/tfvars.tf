variable "tgw_count" {}
variable "nfw_count" {}
variable "pub_count" {}
variable "vpc_id" {}
variable "env" {}
variable "tags" {
  type = map(string)
}
variable "transit_gateway_id" {}