# # create Ingress & Egress Internet gateways 
resource "aws_internet_gateway" "igw" {
 count   = var.env != "inspection_vpc" ? 1: 0
  vpc_id = var.vpc_id
  tags   = merge(var.tags, { "Name" = format("%s-IGW", var.env) })
}

# # # create Egress VPC EIP for each nat gw

resource "aws_eip" "nat_eip" {
  count    = var.env == "egress_vpc"  ? length(var.pub_count) : 0
  vpc      = true
  tags = merge(
    var.tags,
    tomap({
      "Name" = format("%s-EIP-%s",var.env,count.index+1)
    })
  )
}


resource "aws_nat_gateway" "nat" {
  count = var.env == "egress_vpc"  ? length(var.pub_count) : 0
  allocation_id = aws_eip.nat_eip[count.index].id
  subnet_id     = element(tolist(data.aws_subnet_ids.egress_pub[0].ids),count.index)

  tags = merge(
    var.tags,
    tomap({
      "Name" = format("%s-NAT-%s",var.env,count.index+1)
    })
  )
}