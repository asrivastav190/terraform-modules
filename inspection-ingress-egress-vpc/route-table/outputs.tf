output "nfw_ids" {
    value =  [for v in data.aws_subnet_ids.nfw  : v.ids ]
}

output "egress_pub_sub_ids" {
    value = [for v in data.aws_subnet_ids.egress_pub : v.ids]
}

output "prv_tgw_rt_ids" {
  value = aws_route_table.prv_tgw.id
}

output "inspection_nfw_rt_id" {
  value = aws_route_table.inspection_prv_nfw[*].id
}

output "pub_rt_id" {
  value = aws_route_table.public[*].id
}