locals {
  inspection_env     = var.global["inspection_env"]
  ingress_env        = var.global["ingress_env"]
  egress_env         = var.global["egress_env"]
  region             = var.global["region"]
  tags               = var.tags
  vpc_cidr           = "${var.cidr_ab}.0.0/16"
  prv_tgw_subnet     = format("%s-Prv-TGW", var.vpc_name)
  prv_nfw_inspection = format("%s-Prv-NFW", local.inspection_env)
}