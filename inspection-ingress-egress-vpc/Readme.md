# AWS Inspection VPC
## Domain: Terraform
### Service: VPC
#### Description
●	The Inspection VPC consists of two subnets in each AZ. One subnet is a dedicated firewall endpoint subnet and the second is dedicated to an AWS Transit Gateway attachment.
●	Each Transit Gateway subnet requires a dedicated VPC route table to ensure the traffic is forwarded to the firewall endpoint.
●	These route tables have a default route (0.0.0.0/0) pointing towards the firewall endpoint in the same AZ.
●	For the return traffic from the firewall endpoint, a single VPC route table is configured. The route table contains a default route towards AWS Transit Gateway. 
●	Traffic is returned to the AWS Transit Gateway in the same AZ after it has been inspected by AWS Network Firewall.
#### Terraform Module
##### +---modules
##### |   +---inspection-ingress-egress-vpc
##### |   |       gw.tf
##### |   |       locals.tf
##### |   |       outputs.tf
##### |   |       providers.tf
##### |   |       route_table.tf
##### |   |       sg.tf
##### |   |       subnets.tf
##### |   |       tfvars.tf
##### |   |       vpc-tgw-attachment.tf
##### |   |       vpc.tf


gw.tf: Not applicable in the case of Inspection VPC.
  
locals.tf: Locals are named values that you can refer to in your configuration. In this case we are passing environment name, region, tag values, subnet name and availability zones.

outputs.tf: This is responsible for logging the output of the Inspection VPC ID, it’s subnets, VPC flow log ID and attachment ID of VPC and Transit Gateway.

route_table.tf: This Terraform file is responsible for creating route tables of both the private transit subnet and firewall subnet. Below is a reference screenshot:

 ![Image](../images/vpc/inspection-ingress-egress/inspection_rt.png)

subnets.tf: This Terraform file creates the transit and firewall subnet for the inspection VPC according to its defined AZs. Below is a reference screenshot:
 
![Image](../images/vpc/inspection-ingress-egress/inspection_subnet.png)

tfvars.tf: This Terraform file is used to pass input variables like the Inspection VPC CIDR range, tags, environment, availability zone, Transit Gateway ID, its route table and NACL rules. All these parameters are passed by the root folder variables.tf file.

vpc-tgw-attachement.tf: This file is responsible for creating the attachment between the Inspection VPC and Transit Gateway, it then associates the attachment to the firewall route table of the Transit Gateway.

vpc.tf: Terraform file used to create the Inspection VPC.
####  Root Structure
##### | |   main.tf
##### | |   variables.tf
##### | |   outputs.tf

main.tf: This file is responsible for passing variables required to call the inspection-ingress-egress-vpc module and create its resources as defined above in a specified AWS account.

inspection-ingress-egress-vpc: This module creates its resources as defined above in a specified AWS account.

variables.tf: Variables for public and private NACL rules passed specifically for the inspection-ingress-egress-vpc module. Please note all of these variables can be customized, see screenshot below.

 ![Image](../images/vpc/inspection-ingress-egress/variable.png)

#### Reference Links
N/A

#### Diagram
 ![Image](../images/vpc/inspection-ingress-egress/arch_snap.png)

#### Limitations
N/A

________________________________________
# AWS Ingress VPC
## Domain: Terraform
### Service: VPC
#### Description
●	The Ingress VPC consists of two subnets in each AZ. One subnet is dedicated to the AWS Transit Gateway attachment and the second is a public subnet.
●	Each Transit Gateway subnet requires a dedicated VPC route table to ensure the traffic is forwarded to the Transit Gateway via internet gateway in the public subnet. 
●	Traffic is then forwarded to the Ingress VPC.
●	These route tables have route (10.0.0.0/8) pointing towards the Transit Gateway in the same AZ.
#### Terraform Module
##### +---modules
##### |   +---inspection-ingress-egress-vpc
##### |   |       gw.tf
##### |   |       locals.tf
##### |   |       outputs.tf
##### |   |       providers.tf
##### |   |       route_table.tf
##### |   |       subnets.tf
##### |   |       tfvars.tf
##### |   |       vpc-tgw-attachment.tf
##### |   |       vpc.tf

gw.tf: Used to create an internet gateway to be associated with the Ingress public subnet.
  
locals.tf: Locals are named values that you can refer to in your configuration. In this case we are passing environment name, region, tag values, subnet name and availability zones.

outputs.tf: This is responsible for logging the Ingress VPC ID, it’s subnets, VPC flow log ID and attachment ID of VPC and Transit Gateway.

route_table.tf: This Terraform file is responsible for creating route tables for both the private Transit Gateway subnet and public subnet. Below is a reference screenshot:

 ![Image](../images/vpc/inspection-ingress-egress/ingress_rt.png)

subnets.tf: This Terraform file creates the Transit Gateway and public subnet for the Ingress VPC according to its respective AZ. Below is a reference screenshot:

 ![Image](../images/vpc/inspection-ingress-egress/ingress_subnet.png)

tfvars.tf: This Terraform file is used to pass input variables like Ingress VPC CIDR range , tags , environment , availability zone, Transit Gateway ID, its route table and NACL rules. All these parameters are passed by the root folder variables.tf file.

vpc-tgw-attachement.tf: This file is responsible for creating an attachment between the Ingress VPC and Transit Gateway. This associates the attachment to the spoke route table of the Transit Gateway and propagates in the Transit Gateway’s firewall route table.

vpc.tf: Terraform file used to create the Ingress VPC.
#### Root Structure
##### | |   main.tf
##### | |   variables.tf
##### | |   outputs.tf

main.tf: This file is responsible for passing variables required to call the inspection-ingress-egress-vpc module and create its resources as defined above in a specified AWS account.

variables.tf: Public and private NACL rule variables are passed specifically for the inspection-ingress-egress-vpc module.
Please note all of these variables can be customized and below is a reference screenshot:

![Image](../images/vpc/inspection-ingress-egress/variable.png) 

#### Reference Links
N/A

#### Diagram
 ![Image](../images/vpc/inspection-ingress-egress/arch_snap.png)

#### Limitations
N/A
________________________________________
# AWS Egress VPC
## Domain: Terraform
### Service: VPC
#### Description
●	The Egress VPC consists of two subnets in each AZ. One subnet is dedicated to the AWS Transit Gateway attachment and the second is the public subnet.
●	Each Transit Gateway subnet requires a dedicated VPC route table attached to the NAT and Transit Gateway whereas the public subnet is attached to the Transit Gateway and the internet gateway.
●	These route tables have route (10.0.0.0/8) pointing towards the Transit Gateway in the same AZ.
●	The configuration centralizes outbound internet traffic from many VPCs without compromising VPC isolation.
#### Terraform Module
##### +---modules
##### |   +---inspection-ingress-egress-vpc
##### |   |       gw.tf
##### |   |       locals.tf
##### |   |       outputs.tf
##### |   |       providers.tf
##### |   |       route_table.tf
##### |   |       subnets.tf
##### |   |       tfvars.tf
##### |   |       vpc-tgw-attachment.tf
##### |   |       vpc.tf

gw.tf: Used to create an internet gateway to be associated with the egress public subnet.
  
locals.tf: Locals are named values that you can refer to in your configuration. In this case we are passing environment name, region, tag values, subnet name and availability zones.

outputs.tf: This is responsible for logging the Egress VPC ID, it’s subnets, VPC flow log ID and attachment ID of the VPC and Transit Gateway.

route_table.tf: This Terraform file is responsible for creating route tables for both the private Transit Gateway subnet and public subnet. Below is a reference screenshot:

 ![Image](../images/vpc/inspection-ingress-egress/egress_rt.png)

subnets.tf: This Terraform file creates the transit and public subnets for the Egress VPC according to its respective AZ. Below is a reference screenshot:

 ![Image](../images/vpc/inspection-ingress-egress/egress_subnet.png)

tfvars.tf: This Terraform file is used to pass input variables like the Egress VPC CIDR range, tags , environment, availability zone, Transit Gateway ID with it’s route table, VPC flow logs which can be enabled/disables as per the requirement, and NACL rules. All these parameters are passed by the root folder variables.tf file.

vpc-tgw-attachement.tf: This file is responsible for creating the attachment between the Egress VPC and Transit Gateway associating the attachment to the spoke route table of the Transit Gateway.

vpc.tf: Terraform file used to create the Egress VPC.
#### Root Structure
##### | |   main.tf
##### | |   variables.tf
##### | |   outputs.tf

main.tf: This file is responsible for passing variables required to call the inspection-ingress-egress-vpc module and create its resources as defined above in a specified AWS account.

variables.tf: Variables passed specifically for the inspection-ingress-egress-vpc module are:
enable/disable VPC flow logs for inspection, Ingress and Egress VPCs, along with public and private NACL rules. Please note all of these variables can be customized and below is a reference screenshot:

 ![Image](../images/vpc/inspection-ingress-egress/variable.png)

#### Reference Links
N/A

#### Diagram
 
![Image](../images/vpc/inspection-ingress-egress/arch_snap.png)

#### Limitations
N/A

---
___