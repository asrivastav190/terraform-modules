
output "s3_endpoint_id" {
  value       = aws_vpc_endpoint.s3.*.id
  description = "The ID of the s3 endpoint."
}

output "s3_endpoint_prefix_list_id" {
  value       = aws_vpc_endpoint.s3.*.prefix_list_id
  description = "The prefix list ID of the s3 endpoint."
}

