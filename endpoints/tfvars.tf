variable "app_env" {
  description = "Stage (e.g. `prod`, `dev`, `staging`)"
  type        = string
}

variable "app_name" {
  description = "Name  (e.g. `app` or `cluster`)"
  type        = string
}

variable "vpc_id" {
  type    = list(any)
  default = []
}

variable "type" {
  type        = string
  description = "Type of subnet, to get single subnet per az - for associating with ssm endpoint (only `private`)"
  default     = "private"
}

variable "enable_s3_endpoint" {
  description = "Should be true if you want to provision an S3 endpoint to the VPC"
  default     = true
}

variable "private_route_tables" {
  type    = list(string)
  default = []
}

variable "private_subnet_ids_per_az" {
  type    = list(string)
  default = []
}

variable "tags" {
  type = map(string)
}
/*
variable "endpoint_name" {
  type        = string
  description = "Name for the requested endpoint"
}

variable "vpc_cidr_block" {
  type        = string
  description = "Base CIDR block which is divided into subnet CIDR blocks (e.g. `10.0.0.0/24`)"
}


variable "enable_ssm_endpoint" {
  description = "Should be true if you want to provision an SSM endpoint to the VPC"
  default     = false
}


variable "ssm_endpoint_private_dns_enabled" {
  description = "Whether or not to associate a private hosted zone with the specified VPC for SSM endpoint"
  default     = true
}

variable "enable_ssmmessages_endpoint" {
  description = "Should be true if you want to provision a ssmmessages endpoint to the VPC"
  default     = false
}

variable "enable_ec2messages_endpoint" {
  description = "Should be true if you want to provision an ec2messages endpoint to the VPC"
  default     = false
}

variable "enable_logs_endpoint" {
  description = "Should be true if you want to provision an Cloudwatch logs endpoint to the VPC"
  default     = false
}

*/


