## Gateway Endpoint
## Domain:Terraform
## Service: Endpoint for Amazon S3

## Description

A gateway endpoint is a target in your route table used for traffic destined to either Amazon S3 or DynamoDB.

## Terraform Module
## +---endpoints
## ||   Readme.md
## ||   endpoint.tf
## ||   outputs.tf
## ||   tfvars.tf


endpoint.tf: This Terraform file creates an S3 (gateway) endpoint with respect to VPC and attaches its route table association to the endpoint.


![Image](../images/endpoint/endpoint_tf.PNG)

outputs.tf: Displays S3 endpoint IDs

![Image](../images/endpoint/output_tf.PNG)

tfvars.tf: This is a variable file for configuring the acceptance of VPC IDs, enable/disables the S3 endpoint, private route table ID, and subnet IDs.


![Image](../images/endpoint/tfvars.PNG)

## Root Structure
##  |   main.tf
##  |   outputs.tf
##  |   providers.tf
##  |   variables.tf

Main.tf: This file is responsible for passing variables required to create the required endpoints.



![Image](../images/endpoint/main_tf.PNG)


Note: You will need to add the module output of the VPC and route table in a list under vpc_id and private_route_tables for the new VPC.

##### Reference Links
There are no charges for using gateway endpoints.
https://docs.aws.amazon.com/vpc/latest/privatelink/vpc-endpoints-s3.html


##### Diagram
N/A

##### Limitations
N/A
