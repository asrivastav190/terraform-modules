######################
# VPC Endpoint for S3
######################

data "aws_vpc_endpoint_service" "s3" {
  count = var.enable_s3_endpoint ? 1 : 0

  service      = "s3"
  service_type = "Gateway"
}

resource "aws_vpc_endpoint" "s3" {
  #count = var.enable_s3_endpoint ? 1 : 0
  count        = var.enable_s3_endpoint ? length(var.vpc_id) : 0
  vpc_id       = element(var.vpc_id, count.index)
  service_name = element(data.aws_vpc_endpoint_service.s3.*.service_name, count.index)
  tags         = var.tags
}

resource "aws_vpc_endpoint_route_table_association" "private_s3" {
  count           = var.enable_s3_endpoint ? length(var.private_route_tables) : 0
  vpc_endpoint_id = element(aws_vpc_endpoint.s3.*.id, count.index)
  route_table_id  = element(var.private_route_tables, count.index)
}