# #### SERVICE_CONTROL_POLICIES

# ## Deny Marketplace Access
resource "aws_organizations_policy" "deny_marketplace_access" {
  name = var.marketplace_rule_name
  description = "Denies AWS Marketplace Access except if you have the role allowed-marketplace"
  content = data.aws_iam_policy_document.deny_marketplace_access.json
  # tags   = var.tags
}

resource "aws_organizations_policy_attachment" "deny_marketplace_access" {
  policy_id = aws_organizations_policy.deny_marketplace_access.id
  target_id = data.aws_organizations_organizational_units.ou.id
}