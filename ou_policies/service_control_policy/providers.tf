#Below is the required version for aws provider
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.73.0"
      configuration_aliases = [ aws.pwc_explore ]
    }
  }
}

provider "aws" {
  # alias  = "pwc_explore"
  region = "us-east-1"
  # profile = "pwc_explore"
  assume_role {
    role_arn     = var.role_arn
    # session_name = "ou_policies"
  }
}