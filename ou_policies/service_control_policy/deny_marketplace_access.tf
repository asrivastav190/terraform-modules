# ## Deny Marketplace Access
data "aws_iam_policy_document" "deny_marketplace_access" {
    statement {
      sid = "ProcurementPolicy"
  
      actions = [
         "aws-marketplace:*",
      ]
  
      resources = [
        "*",
      ]

      condition {
        test     = "StringNotLike"
        variable = "aws:PrincipalARN"
        values =  [
          "${aws_ssoadmin_permission_set.allowed_marketplace.arn}"
        ]
      }
  
      effect = "Deny"
    }
  }