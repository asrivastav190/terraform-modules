variable "tags" {
  type    = map(string)
}

variable "tag_policy_name" {}

variable "iso_deployment_artifact_value" {}

variable "iso_environment_value" {}

variable "iso_contact_value" {}

variable "iso_activity_code" {}

variable "name" {}

variable "role_arn" {}