output "root_id" {
  value = data.aws_organizations_organizational_units.ou.id
}

output "tag_policy_id" {
  value = aws_organizations_policy.tag_policy.id
}

data "aws_caller_identity" "current" {}

output "tag_policy_account_id" {
  value = data.aws_caller_identity.current.account_id
}