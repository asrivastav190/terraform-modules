variable "tags" {
  type    = map(string)
}

variable "backup_policy_name" {}

variable "target_backup_vault_name" {
  type = string
  default = "iso-backup-vault"
}

variable "backup_policy_rendered" {
  description = "(Required) to be passed from child account."
  type        = string
  default     = null
}

variable "role_arn" {}