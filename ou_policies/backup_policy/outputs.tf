output "root_id" {
  value = data.aws_organizations_organizational_units.ou.id
}

output "backup_policy_id" {
  value = aws_organizations_policy.backup_policy.id
}


output "backup_policy_account_id" {
  value = data.aws_caller_identity.current.account_id
}

