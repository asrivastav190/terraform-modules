resource "aws_organizations_policy" "backup_policy" {
  name = var.backup_policy_name
  type = "BACKUP_POLICY"
  description = "backup policy"
  tags   = var.tags
  content  = var.backup_policy_rendered
}

resource "aws_organizations_policy_attachment" "backup_policy" {
  policy_id = aws_organizations_policy.backup_policy.id
  target_id = data.aws_organizations_organizational_units.ou.id
}