resource "aws_s3_bucket" "destination" {
  provider = aws.west2
  bucket   = "replica-${var.bucket}-${random_id.suffix.dec}"
  tags     = var.tags
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  versioning {
    enabled = true
  }
  logging {
    target_bucket = aws_s3_bucket.replica_logs_bucket.id
    target_prefix = "replica-${var.bucket}-${random_id.suffix.dec}/logs/"
  }
}
resource "aws_s3_bucket" "replica_logs_bucket" {
  provider = aws.west2
  bucket   = "${var.replica_log_bucket}-${random_id.suffix.dec}"
  acl      = "log-delivery-write"
  tags     = var.tags
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
  versioning {
    enabled = var.versioning
  }

  lifecycle_rule {
    id      = "log"
    enabled = true

    prefix = "log/"

    tags = {
      rule      = "log"
      autoclean = "true"
    }

    transition {
      days          = 30
      storage_class = "STANDARD_IA" # or "ONEZONE_IA"
    }

    transition {
      days          = 90
      storage_class = "GLACIER"
    }

    expiration {
      days = 365
    }
  }
}

resource "aws_s3_bucket_public_access_block" "destination" {
  provider                = aws.west2
  bucket                  = aws_s3_bucket.destination.id
  block_public_acls       = var.block_public_acls
  block_public_policy     = var.block_public_policy
  ignore_public_acls      = var.ignore_public_acls
  restrict_public_buckets = var.restrict_public_buckets
}

resource "aws_s3_bucket_public_access_block" "replica_logs_bucket" {
  provider                = aws.west2
  bucket                  = aws_s3_bucket.replica_logs_bucket.id
  block_public_acls       = var.block_public_acls
  block_public_policy     = var.block_public_policy
  ignore_public_acls      = var.ignore_public_acls
  restrict_public_buckets = var.restrict_public_buckets

}
