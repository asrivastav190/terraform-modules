# Ensure that common_tags get created consistently
locals {
  common_tags = {
    Application = var.app_name
    Environment = var.app_env
  }
}
