#create a bucket
resource "aws_s3_bucket" "this" {
  bucket        = "${var.bucket}-${random_id.suffix.dec}"
  force_destroy = var.force_destroy
  acl           = var.acl
  tags          = var.tags
  # You can record the actions that are taken by users, roles, or AWS services on Amazon S3 resources and maintain log records for auditing and compliance purposes
  logging {
    target_bucket = aws_s3_bucket.logs_bucket.id
    target_prefix = "${var.bucket}-${random_id.suffix.dec}/logs/"
  }

  versioning {
    enabled = var.versioning
  }

  # default server side encryption
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = var.sse_algorithm
      }
    }
  }

  # object lifecycle rule - abort incomplete multi-part uploads
  lifecycle_rule {
    enabled                                = var.enable_abort_incomplete_multipart_upload
    abort_incomplete_multipart_upload_days = var.abort_incomplete_multipart_upload_days
    #}

    noncurrent_version_transition {
      days          = var.noncurrent_version_transition_days
      storage_class = "STANDARD_IA"
    }

  }
  depends_on = [var.s3_depends_on]
  replication_configuration {
    role = aws_iam_role.replication.arn

    rules {
      id     = "foobar"
      status = "Enabled"

      filter {
        tags = {}
      }
      destination {
        bucket        = aws_s3_bucket.destination.arn
        storage_class = "STANDARD"

      }
    }
  }
}

# Attach policy to S3 bucket
resource "aws_s3_bucket_policy" "this" {
  #depends_on = [aws_s3_bucket_public_access_block.this]
  count  = var.attach_policy == true ? 1 : 0
  bucket = aws_s3_bucket.this.id
  policy = var.s3_policy_rendered
}


#S3 bucket restriction to block public access
resource "aws_s3_bucket_public_access_block" "this" {
  depends_on              = [aws_s3_bucket_policy.this]
  bucket                  = aws_s3_bucket.this.id
  block_public_acls       = var.block_public_acls
  block_public_policy     = var.block_public_policy
  ignore_public_acls      = var.ignore_public_acls
  restrict_public_buckets = var.restrict_public_buckets
}

#bucket creation for server access log
resource "aws_s3_bucket" "logs_bucket" {
  bucket = "${var.log_bucket}-${random_id.suffix.dec}"
  acl    = "log-delivery-write"
  tags   = var.tags
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }
  versioning {
    enabled = var.versioning
  }
  lifecycle_rule {
    id      = "log"
    enabled = true

    prefix = "log/"

    tags = {
      rule      = "log"
      autoclean = "true"
    }

    transition {
      days          = 30
      storage_class = "STANDARD_IA" # or "ONEZONE_IA"
    }

    transition {
      days          = 90
      storage_class = "GLACIER"
    }

    expiration {
      days = 365
    }
  }
}
resource "aws_s3_bucket_public_access_block" "logs_bucket" {
  depends_on              = [aws_s3_bucket_policy.this]
  bucket                  = aws_s3_bucket.logs_bucket.id
  block_public_acls       = var.block_public_acls
  block_public_policy     = var.block_public_policy
  ignore_public_acls      = var.ignore_public_acls
  restrict_public_buckets = var.restrict_public_buckets
}

resource "random_id" "suffix" {
  byte_length = 8
}