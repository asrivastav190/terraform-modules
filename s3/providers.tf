provider "aws" {
  region  = "us-east-1"
  profile = "logging"
}

provider "aws" {
  region  = "us-west-2"
  profile = "logging"
  alias   = "west2"
}
