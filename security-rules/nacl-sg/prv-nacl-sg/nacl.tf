# /*
# This terraform template will create the following resources,
# 1. NACL
# */

# ## Private NACL
resource "aws_network_acl" "private" {
  vpc_id     = var.vpc_id
  subnet_ids = var.private_subnets
  tags       = merge(var.tags, { "Name" = "${var.vpc_name}-NACL-Private" })
}

resource "aws_network_acl_rule" "private" {
  network_acl_id = aws_network_acl.private.id
  for_each       = var.private_nacl_rules
  rule_action    = each.value.rule_action
  rule_number    = each.value.rule_number
  from_port      = each.value.from_port
  to_port        = each.value.to_port
  protocol       = each.value.protocol
  cidr_block     = each.value.cidr_block
  egress         = each.value.egress
}