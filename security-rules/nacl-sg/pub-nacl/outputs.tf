
output "public_nacl_id" {
  description = "Public NACL ID of $${var.vpc_name}"
  value = aws_network_acl.public.id
}