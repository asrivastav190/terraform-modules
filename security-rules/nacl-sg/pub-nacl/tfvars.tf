variable "vpc_id" {}
variable "vpc_name" {}
variable "public_subnets" {}
variable "tags" {
  type = map(string)
}
variable "public_nacl_rules" {
  type = map(map(string))
}