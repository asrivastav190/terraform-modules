resource "aws_networkfirewall_rule_group" "stateless" {
  for_each = var.stateless_rule_group
  capacity = each.value["capacity"]
  name     = "${each.key}-RG-${var.vpc_id}"
  type     = "STATELESS"
  rule_group {
    rules_source {
      stateless_rules_and_custom_actions {
        stateless_rule {
          priority = each.value["priority"]
          rule_definition {
            actions = [each.value["actions"]]
            match_attributes {
              source_port {
                from_port = each.value["source_from_port"]
                to_port   = each.value["source_to_port"]
              }
              source {
                address_definition = each.value["source_address"]
              }
              destination_port {
                from_port = each.value["destination_from_port"]
                to_port   = each.value["destination_to_port"]
              }
              destination {
                address_definition = each.value["destination_address"]
              }
              protocols = [each.value["protocol"]]
            }
          }
        }
      }
    }
  }
}

resource "aws_networkfirewall_rule_group" "stateful" {
  for_each = var.stateful_rule_group
  capacity = each.value["capacity"]
  name     = "${each.key}-RG-${var.vpc_id}"
  type     = "STATEFUL"
  rule_group {
    rules_source {
      stateful_rule {
        action = each.value["action"]
        header {
          destination      = each.value["destination"]
          destination_port = each.value["destination_port"]
          direction        = each.value["direction"]
          protocol         = each.value["protocol"]
          source           = each.value["source"]
          source_port      = each.value["source_port"]
        }
        rule_option {
          keyword = "sid:1"
        }
      }
    }
  }
}