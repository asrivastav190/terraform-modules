output "firewall_id" {
  value = aws_networkfirewall_firewall.public_workload_vpc_nfw.id
}

output "endpoint_id" {
  value = [for value in flatten(aws_networkfirewall_firewall.public_workload_vpc_nfw[*].firewall_status[0].sync_states) : value.attachment[0].endpoint_id]
}

