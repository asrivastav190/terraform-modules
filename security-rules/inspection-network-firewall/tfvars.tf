variable "vpc_id" {
  description = "Inspection VPC ID"
  default     = {}
}

variable "nfw_subnet" {
  description = "Network firewall subnet IDs"
  #   default = {}
}

variable "subnet_id" {}

variable "inspection_env" {
  description = "Environment name"
}

variable "inspection_vpc_cidr" {
  description = "Inspection VPC CIDR range"
  # default     = "10.161.0.0/16"
}

variable "tags" {
  type = map(string)
}

variable "stateless_rule_group" {
  type    = map(map(string))
  default = {}
}

variable "stateful_rule_group" {
  type    = map(map(string))
  default = {}
}