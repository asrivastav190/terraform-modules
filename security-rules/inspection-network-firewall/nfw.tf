resource "aws_networkfirewall_firewall" "inspection_vpc_nfw" {
  name                = "${local.inspection_env}-NetworkFirewall"
  firewall_policy_arn = aws_networkfirewall_firewall_policy.nfw_policy.arn
  vpc_id              = var.vpc_id

  dynamic "subnet_mapping" {
    for_each = var.nfw_subnet

    content {
      subnet_id = subnet_mapping.value
    }
  }
  tags = local.tags
}