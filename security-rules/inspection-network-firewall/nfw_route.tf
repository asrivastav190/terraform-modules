data "aws_route_table" "selected" {
  subnet_id = var.subnet_id
}

resource "aws_route" "route" {
  route_table_id         = data.aws_route_table.selected.id
  destination_cidr_block = "0.0.0.0/0"
  vpc_endpoint_id        = element([for ss in tolist(aws_networkfirewall_firewall.inspection_vpc_nfw.firewall_status[0].sync_states) : ss.attachment[0].endpoint_id], 0)
}