output "firewall_id" {
  value = aws_networkfirewall_firewall.inspection_vpc_nfw.id
}

output "vpce_ids" {
  value = [for ss in tolist(aws_networkfirewall_firewall.inspection_vpc_nfw.firewall_status[0].sync_states) : ss.attachment[0].endpoint_id]
}