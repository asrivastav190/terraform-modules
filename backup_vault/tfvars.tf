#
# AWS Backup vault
#
variable "vault_name" {
  description = "Name of the backup vault to create. If not given, AWS use default"
  type        = string
  default     = null
}

variable "enable_vault" {
  description = "Should be true if you want to create vault"
}

variable "tags" {
  type = map(string)
}