## AWS Transit Gateway

## Description

A Transit Gateway is a network transit hub that you can use to interconnect your virtual private clouds (VPCs) and on-premises networks. As your cloud infrastructure expands globally, inter-Region peering connects transit gateways together using the AWS Global Infrastructure. Your data is automatically encrypted and never travels over the public internet.

Terraform Module Structure

The Transit Gateway module is structured under the module directory and will be called from main.tf in the root directory. Refer below snap for module structure.

##### Terraform Module
+---transit-gateway
|    data.tf
|    outputs.tf
|    providers.tf
|    readme.md
|    tfvars.tf
|    transit_gateway.tf
|    transit_gateway_rt.tf


Module Call

The Transit Gateway module will be called from the root module as shown below.
```hcl
module "create_transit_gateway_infra" {
  providers = {
    aws.tgw     = aws.use1
    aws.peertgw = aws.usw2
  }
  source                            = "../modules/transit-gateway"
  transit_gateway_name              = var.transit_gateway_name
  peer_transit_gateway_name         = var.peer_transit_gateway_name
  central_egress_vpc_attachement_id = module.create_inspection_ingress_egress_vpcs.central_egress_vpc_tgw_attachment_id
  inspection_vpc_tgw_attachement_id = module.create_inspection_ingress_egress_vpcs.inspection_vpc_tgw_attachment_id
  tags                              = var.tags
}
```
Details:
Providers: We are passing two providers in this module. First for us-east-1 region and second for us-west-2 region.
User Inputs:
Below two use inputs need to be passed as per user requirement.
Transit_gateway_name
Peer_transit_gateway_name 
You can check and update these variables' default values in the variables.tf file in the root directory.
```hcl
###Transit Gateway variable declaration/definition
variable "transit_gateway_name" {
  type        = string
  description = "Name of Transit Gateway in each region"
  default     = "transit-gateway-use1"
}
variable "peer_transit_gateway_name" {
  type        = string
  description = "Peer transit gateway name in us-west-2 region"
  default     = "peer-transit-gateway-usw2"
}
```
Automated Inputs:
Central_egress_vpc_attachment_id: Identifier for central egress to transit gateway attachment.
Central_ingress_vpc_attachment_id: Identifier for central ingress to transit gateway attachment.
Core Module:
Transit gateway core module is structured as below.


|   +---transit-gateway
|   |       data.tf
|   |       outputs.tf
|   |       providers.tf
|   |       readme.md
|   |       tfvars.tf
|   |       transit_gateway.tf
|   |       transit_gateway_rt.tf


Default attribute values for transit gateway creation are declared in tfvars.tf file inside modules-🡪 transit-gateway directory. 

```hcl
#Below Input Variables are declared and defined.These values are not expected to be passed from calling module.
 
variable "amazon_side_asn" {
  description = "Private Autonomous System Number (ASN) for the Amazon side of a BGP session"
  type        = string
  default     = "64512"
}
 
variable "enable_auto_accept_shared_attachments" {
  description = "Whether resource attachment requests are automatically accepted."
  type        = bool
  default     = true
}
 
variable "enable_default_route_table_association" {
  description = "Whether resource attachments are automatically associated with the default association route table"
  type        = bool
  default     = false
}
 
variable "enable_default_route_table_propagation" {
  description = "Whether resource attachments automatically propagate routes to the default propagation route table"
  type        = bool
  default     = true
}
 
variable "enable_dns_support" {
  description = "Whether DNS support is enabled"
  type        = bool
  default     = true
}
 
variable "enable_vpn_ecmp_support" {
  description = "Whether VPN Equal Cost Multipath Protocol support is enabled"
  type        = bool
  default     = true
}
```
### Resources:

The Transit Gateway module will create the following resources.

Transit Gateway in the us-east-1 region.
Transit Gateway in the us-west-2 region.

Inter region transit Gateway peering attachment between above two transit gateways
Inter region peering acceptor.
Transit Gateway Route Tables
Spoke VPC Inspection Route Table: This Transit Gateway route table will be associated with all spoke/workload VPC and central Ingress VPC. This route table will have a route as shown below.

```
 CIDR    |  Attachment 
------------|------------
0.0.0.0/0 | Inspection VPC Attachment ID
```



The above default route implies that any traffic coming from any spoke/workload VPC or ingress VPC will be forwarded to the inspection VPC for firewall inspection.
The firewall subnet Transit Gateway route table: This Transit Gateway route table will be associated with the Inspection VPC attachment and will contain the routes as shown below-

```
 CIDR    |  Attachment 
------------|------------
10.164.0.0/16|Research VPC attachment ID
10.165.0.0/16|Development VPC attachment ID
10.166.0.0/16|Production VPC attachment id
10.167.0.0/16|Integration VPC attachment id
10.162.0.0/16|Central Ingress VPC attachment id
0.0.0.0/0|Central Egress VPC attachment id
<Yet to be decided>|Us-west-2 peering attachment id
<yet to be decided>|<direct connect gateway attachment>

```

The above route table will route incoming traffic for firewall inspection based on its destination CIDR, one default route will go to the Central Egress VPC which implies that traffic is intended for the internet, hence forwarded to Central Egress VPC.
All routes except the default route to the Central Egress VPC will be automatically propagated when the VPC is attached to the Transit Gateway. This VPC attachment is created in the VPC modules. 

#### Reference Links
https://docs.aws.amazon.com/vpc/latest/tgw/what-is-transit-gateway.html

#### Diagram

We have developed the Terraform module to implement the below proposed architecture.

 ![Image](../images/transit_gateway/transit_gateway.PNG)


##### Limitations
N/A
