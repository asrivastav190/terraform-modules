#Below Route Table will be associated with all Spoke VPC attachment and Ingress VPC attachemnt

resource "aws_ec2_transit_gateway_route_table" "spoke_vpc_inspection_tgw_route_table" {
  transit_gateway_id = aws_ec2_transit_gateway.iso_ne_transit_gateway.id
  tags               = merge(var.tags, { "Name" = "Spoke VPC Inspection Route Table" })
  provider           = aws.tgw
}

#Below Route Table will be associated with Firewall VPC attachment

resource "aws_ec2_transit_gateway_route_table" "firewall_subnet_tgw_route_table" {
  transit_gateway_id = aws_ec2_transit_gateway.iso_ne_transit_gateway.id
  tags               = merge(var.tags, { "Name" = "firewall_subnet_tgw_route_table" })
  provider           = aws.tgw
}