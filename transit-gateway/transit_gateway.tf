#Create first Transit Gateway in us-east-2 region

resource "aws_ec2_transit_gateway" "iso_ne_transit_gateway" {
  description                     = "Transit gateway created for hub and spoke vpc model"
  amazon_side_asn                 = var.amazon_side_asn
  auto_accept_shared_attachments  = var.enable_auto_accept_shared_attachments ? "enable" : "disable"
  default_route_table_association = var.enable_default_route_table_association ? "enable" : "disable"
  default_route_table_propagation = var.enable_default_route_table_propagation ? "enable" : "disable"
  dns_support                     = var.enable_dns_support ? "enable" : "disable"
  vpn_ecmp_support                = var.enable_vpn_ecmp_support ? "enable" : "disable"
  tags                            = merge(var.tags, { "Name" = "${var.transit_gateway_name}" })
  provider                        = aws.tgw
}

#create second transit gateway in us-west-2 region

resource "aws_ec2_transit_gateway" "iso_ne_peer_transit_gateway" {
  description                     = "Transit gateway created for hub and spoke vpc model in second region for peering"
  amazon_side_asn                 = var.amazon_side_asn
  auto_accept_shared_attachments  = var.enable_auto_accept_shared_attachments ? "enable" : "disable"
  default_route_table_association = var.enable_default_route_table_association ? "enable" : "disable"
  default_route_table_propagation = var.enable_default_route_table_propagation ? "enable" : "disable"
  dns_support                     = var.enable_dns_support ? "enable" : "disable"
  vpn_ecmp_support                = var.enable_vpn_ecmp_support ? "enable" : "disable"
  tags                            = merge(var.tags, { "Name" = "${var.peer_transit_gateway_name}" })
  provider                        = aws.peertgw
}

###Create Peering between the above two transit gateway created in us-east-1 and us-west-2 region

resource "aws_ec2_transit_gateway_peering_attachment" "use1_usw2_peering_attachment" {
  peer_account_id         = aws_ec2_transit_gateway.iso_ne_peer_transit_gateway.owner_id
  peer_region             = data.aws_region.peer_region.name
  peer_transit_gateway_id = aws_ec2_transit_gateway.iso_ne_peer_transit_gateway.id
  transit_gateway_id      = aws_ec2_transit_gateway.iso_ne_transit_gateway.id
  tags                    = merge(var.tags, { "Name" = "use1-usw2-tgw-peering-attachment" })
  provider                = aws.tgw
}

###Accept the peering region in us-west-2 region

resource "aws_ec2_transit_gateway_peering_attachment_accepter" "use1_usw2_peering_attachment_acceptor" {
  transit_gateway_attachment_id = aws_ec2_transit_gateway_peering_attachment.use1_usw2_peering_attachment.id
  tags                          = merge(var.tags, { "Name" = "USE1-USW2-attachment-acceptor" })
  provider                      = aws.peertgw
}


