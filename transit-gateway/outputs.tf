output "transit_gateway_id" {
  value       = aws_ec2_transit_gateway.iso_ne_transit_gateway.id
  description = "EC2 Transit Gateway identifier"
}

output "peer_tgw_owner_id" {
  value       = aws_ec2_transit_gateway.iso_ne_peer_transit_gateway.owner_id
  description = "Identifier of the AWS account that owns the EC2 Transit Gateway"
}
output "transit_gateway_arn" {
  value       = aws_ec2_transit_gateway.iso_ne_transit_gateway.arn
  description = "EC2 Transit Gateway Amazon Resource Name (ARN)"
}

output "spoke_vpc_inspection_tgw_route_table_id" {
  value       = aws_ec2_transit_gateway_route_table.spoke_vpc_inspection_tgw_route_table.id
  description = "Transit Gateway Route Table identifier"
}

output "firewall_subnet_tgw_route_table_id" {
  value       = aws_ec2_transit_gateway_route_table.firewall_subnet_tgw_route_table.id
  description = "Transit Gateway Route Table identifier for the Firewall Subnet TGW Route Table"
}