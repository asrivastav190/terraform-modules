data "aws_region" "current" {}
data "aws_subnet_ids" "tgw" {
  vpc_id = var.vpc_id
  tags = {
    Name = "*-TGW-*"
  }
}