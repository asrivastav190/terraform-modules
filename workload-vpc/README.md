The requirement is to create a VPC with below components:

-General private subnets spread across 4 availability zones having /24 CIDR.
-Dedicated Transit Gateway attachment subnets with /28 CIDR.
-All egress traffic must be routed through the Inspection VPC firewall using the Transit Gateway.
-VPC Flow log’s need to be enabled.
-A S3 endpoint for each VPC.
-As per initial deployment, we are creating the four workload VPCs.

    --Development VPC
    --Integration VPC
    --Research VPC
    --Production VPC

##### Terraform Module

\---workload-vpc
|        data.tf
|       locals.tf
|       outputs.tf
|       providers.tf
|       README.md
|       route_table.tf
|       subnets.tf
|       tfvars.tf
|       vpc-tgw-attachment.tf
|       vpc.tf

This workload VPC module will be called from main.tf in the root directory as shown below.

'module "create_workload_vpc" {
  providers = {
    aws = aws.use1
  }
  for_each                                = var.spoke_vpc_param
  vpc_name                                = each.value["vpc_name"]
  vpc_cidr                                = each.value["vpc_cidr"]
  source                                  = "../modules/workload-vpc"
  transit_gateway_id                      = module.create_transit_gateway_infra.transit_gateway_id
  spoke_vpc_inspection_tgw_route_table_id = module.create_transit_gateway_infra.spoke_vpc_inspection_tgw_route_table_id
  firewall_subnet_tgw_route_table_id      = module.create_transit_gateway_infra.firewall_subnet_tgw_route_table_id
  private_nacl_rules                      = var.private_nacl_rules
  tags                                    = var.tags
  enable_az_workload_vpc                  = var.enable_az_workload_vpc
  az_mapping                              = var.az_mapping
  az_nums                                  = var.az_nums
}'

User Inputs: 
------------
The below User Inputs are required and can be customized from the variables.tf file in the root directory.

##Workload VPC VPC Name and CIDR definition
variable "spoke_vpc_param" {
  type = map(object({
    vpc_name = string
    vpc_cidr = string
  }))
  default = {
    production_vpc = {
      vpc_cidr = "10.166.0.0/16"
      vpc_name = "production_vpc"
    },
    development_vpc = {
      vpc_cidr = "10.165.0.0/16"
      vpc_name = "development_vpc"
    },
    research_vpc = {
      vpc_cidr = "10.164.0.0/16"
      vpc_name = "research_vpc"
    },
    integration_vpc = {
      vpc_cidr = "10.167.0.0/16"
      vpc_name = "integration_vpc"
    }
    ### Add another VPC name and CIDR here that need to be created as spoke VPC design
  }
}
 
variable "enable_az_workload_vpc" {
  description = "List of Availability Zones to be initialized in Workload VPC"
  type        = set(string)
  default     = ["a", "b", "c", "d"]
}
####Below variable need to be updated based on the AWS account you are working in
variable "az_mapping" {
  description = "Mapping of AWS region to physical data center"
  default = {
    "us-east-1a" = "use1-az4"
    "us-east-1b" = "use1-az6"
    "us-east-1c" = "use1-az1"
    "us-east-1d" = "use1-az2"
  }
}
 
variable "az_nums" {
  type = map(string)
  default = {
    a = 1
    b = 2
    c = 3
    d = 4
  }
}

VPC Name and VPC CIDR: You can provide a VPC name and CIDR mapping in the variable spoke_vpc_param. As part of initial deployment, we are creating four spoke VPCs. If any new VPCs are required the same can be added in the variable map.

enable_az_workload_vpc: This variable takes the input for how many Availability Zones you want to enable for the spoke VPC. As part of proposed architecture, we are deploying subnets in four AZs.

az_mapping: AWS maps logical AZs to different physical data centers in different accounts. This mapping is useful when we are proceeding with a shared VPC architecture. Using the parameter availability_zone_id in place of availability_zone ensures that when we share the subnet, traffic is inside the same physical data center. For each account it can be found in the Resource Access Manager service page.

![image1](../images/workload-vpc/az_mapping.PNG)

***Remember to update this variable as per mapping in your account***

az_nums: This variable is simply the number assigned to the AZ. For example us-east-1a =1, us-east-1b=2 , i.e. a=1, b=2, c=3 and so on. This is used in the Terraform cidrsubnet function to dynamically calculate CIDR for the subnet.

private_nacl_rules: This variable passes the NACL rules defined in the variable.

Automated Inputs: 
-----------------
Below are inputs passed into the module from the outputs of other modules.

spoke_vpc_inspection_tgw_route_table_id: This Transit Gateway route table is associated with the Transit Gateway and spoke VPC attachment. The attachment creation can be found in vpc-tgw-attachement.tf in the workload-vpc module.

firewall_subnet_tgw_route_table_id: This route table is used to propagate the spoke VPC CIDR route to its Transit Gateway route table. 
transit_gateway_id: The ID for the Transit Gateway in the us-east-1 region is passed to attach the Transit Gateway with the created spoke VPC.

Core Module: 
-----------
The core workload/spoke VPC module is structured as shown below.

\---workload-vpc
|        data.tf
|       locals.tf
|       outputs.tf
|       providers.tf
|       README.md
|       route_table.tf
|       subnets.tf
|       tfvars.tf
|       vpc-tgw-attachment.tf
|       vpc.tf


Resources
VPC: A VPC with the passed CIDR range and Name and it's flow logs will be created.

Subnets: Two private subnets in each AZ (four AZs) will be created. Transit Gateway subnets will be a /28 CIDR and other subnets will be a /24 CIDR.

NACL: This is a private NACL, with passive rules which will be created for both kinds of subnets separately. (One for the Transit Gateway attachment subnet and second for general private subnets)

Route Table: The route table will be created for each workload VPC and will be associated with all subnets in that VPC. This route table will have one default route to the Transit Gateway.

Destination | Target
------------|----------
0.0.0.0/0   |Transit Gateway ID


VPC-Transit Gateway attachment: The newly created spoke/workload VPC will be attached to the transit gateway and a VPC CIDR route will be propagated to the spoke_vpc_inspection_tgw_route_table.

The firewall_subnet_tgw_route_table will be associated with this VPC -Transit Gateway attachment.

#### Diagram
This module implenments the below VPC architecture.

![image2](../images/workload-vpc/workloadvpcarch.PNG)

Reference Links
N/A

Limitations
N/A
