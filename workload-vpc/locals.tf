locals {
  region = "us-east-1"
  vpc_cidr = "${var.cidr_ab}.0.0/16"
  prv_tgw_subnet = format("%s-Prv-TGW", var.vpc_name)
}
