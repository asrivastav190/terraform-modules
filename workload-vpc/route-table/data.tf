data "aws_region" "current" {}

data "aws_vpc" "default" {
  id    = var.vpc_id
}

data "aws_subnet_ids" "main" {
  vpc_id = var.vpc_id
#   tags = {
#     Name = "*-${var.subnet_name}-*"
#   }
}