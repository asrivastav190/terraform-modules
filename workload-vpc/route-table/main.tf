resource "aws_route_table" "workload_vpc_rt" {
  vpc_id = var.vpc_id
  route {
    cidr_block         = "0.0.0.0/0"
    transit_gateway_id = var.transit_gateway_id
  }
  tags = merge(var.tags, { "Name" = "${var.vpc_name}-RT-Private" })
}

resource "aws_route_table_association" "workload_vpc_rt_assocation" {
  count          = length(var.subnet_ids)
  subnet_id      = element(tolist(data.aws_subnet_ids.main.ids),count.index)
  route_table_id = aws_route_table.workload_vpc_rt.id
}