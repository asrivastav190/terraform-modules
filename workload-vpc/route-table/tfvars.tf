variable "tags" {
  type = map(string)
}

variable "vpc_id" {}
variable "vpc_name" {
  type        = string
  description = "Name for the requested VPC"
}

variable "transit_gateway_id" {}
variable "subnet_ids" {}