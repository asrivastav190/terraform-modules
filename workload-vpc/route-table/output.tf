output "rt_id" {
  value       = aws_route_table.workload_vpc_rt.id
  description = "Workload VPC route table identifier"
}