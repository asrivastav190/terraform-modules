output "subnet_ids" {
    value = [ for subnet in aws_subnet.default : subnet.id]
}