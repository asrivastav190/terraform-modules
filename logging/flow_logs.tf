# ## Inspection VPC Flow Log
resource "aws_flow_log" "inspection_vpc_log" {
  log_destination      = var.s3_bucket_arn
  log_destination_type = "s3"
  traffic_type         = "ALL"
  vpc_id               = var.inspection_vpc_id
  tags                 = merge(local.tags, { "Name" = format("%s-VpcFlowLogGroup", local.inspection_env) })
}

# ## Ingress VPC Flow Log
resource "aws_flow_log" "ingress_vpc_log" {
  log_destination      = var.s3_bucket_arn
  log_destination_type = "s3"
  traffic_type         = "ALL"
  vpc_id               = var.ingress_vpc_id
  tags                 = merge(local.tags, { "Name" = format("%s-VpcFlowLogGroup", local.ingress_env) })
}

# ## Egress VPC Flow Log
resource "aws_flow_log" "egress_vpc_log" {
  log_destination      = var.s3_bucket_arn
  log_destination_type = "s3"
  traffic_type         = "ALL"
  vpc_id               = var.egress_vpc_id
  tags                 = merge(local.tags, { "Name" = format("%s-VpcFlowLogGroup", local.egress_env) })
}

# ## Workload VPC Flow Log
# resource "aws_flow_log" "workload_vpc_log" {
#   for_each             = var.workload_vpc_ids
#   log_destination      = var.s3_bucket_arn
#   log_destination_type = "s3"
#   traffic_type         = "ALL"
#   vpc_id               = each.value
#   tags                 = merge(local.tags, { "Name" = format("%s-VpcFlowLogGroup", local.workload_env) })
# }