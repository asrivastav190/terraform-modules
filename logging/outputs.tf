output "inspection_vpc_flow_log_id" {
  value = aws_flow_log.inspection_vpc_log.id
}

output "ingress_vpc_flow_log_id" {
  value = aws_flow_log.ingress_vpc_log.id
}

output "egress_vpc_flow_log_id" {
  value = aws_flow_log.egress_vpc_log.id
}

# output "workload_vpc_flow_log_ids" {
#   value = [for value in aws_flow_log.workload_vpc_log : value.id]
# }