# Logging
## Domain: Terraform
### Service: VPC
#### Description 
Enable VPC flow logs to capture network traffic.

#### Terraform modules
##### +---Logging
##### | |       flow_logs.tf
##### | |       locals.tf
##### | |       outputs.tf
##### | |       providers.tf
##### | |       tfvars.tf

flow_logs.tf: This Terraform file enables/disables VPC flow logs for the Inspection, Ingress and Egress VPCs. If enabled, it will publish flow log data to the desired S3 bucket.

locals.tf: Defines the naming convention for each of the resources and tags.

outputs.tf: Displays VPC flog log IDs.

tfvars.tf: This is a variable file accepting VPC IDs, naming convention, to enable/disable VPC flow logs, the S3 bucket ARN to store flow logs and tags.

#### Root Structure
##### | |   main.tf
##### | |   outputs.tf
##### | |   providers.tf
##### | |   variables.tf

main.tf: This file is responsible for passing variables required to call the logging module and create its resources as defined above in a specified AWS account.

 ![Image](../images/logging/main_snapshot.png)

variables.tf: Variables passed specifically for the logging module are enable/disable VPC flow logs for Inspection, Ingress and Egress VPCs, VPC IDs, tags and S3 bucket ARN.

##### Reference Links

[url] https://docs.aws.amazon.com/vpc/latest/userguide/flow-logs.html

##### Diagram
N/A

##### Limitations
N/A


---
___