locals {
  inspection_env = var.global["inspection_env"]
  ingress_env    = var.global["ingress_env"]
  egress_env     = var.global["egress_env"]
  # workload_env   = var.global["workload_env"]
  region         = var.global["region"]
  tags           = var.tags
}