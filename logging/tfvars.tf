variable "inspection_vpc_id" {}
variable "ingress_vpc_id" {}
variable "egress_vpc_id" {}

# variable "workload_vpc_ids" {
#   type = set(string)
# }

variable "global" {
  description = "Global map of variables"
  type        = map(string)
  default = {
    "inspection_env" = "InspectionVPC",
    "ingress_env"    = "IngressVPC",
    "egress_env"     = "EgressVPC",
    # "workload_env"   = "DevelopmentVPC",
    "region"         = "us-east-1"
  }
}

variable "s3_bucket_arn" {}

variable "tags" {
  type = map(string)
}